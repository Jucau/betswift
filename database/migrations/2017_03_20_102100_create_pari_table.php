<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePariTable extends Migration {

	public function up()
	{
		Schema::create('pari', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('match_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->integer('resultat');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('pari');
	}
}