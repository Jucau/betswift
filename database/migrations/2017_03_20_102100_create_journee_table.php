<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJourneeTable extends Migration {

	public function up()
	{
		Schema::create('journee', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nom');
			$table->date('date_limite');
			$table->integer('championnat_id')->unsigned();
			$table->integer('created_by')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('journee');
	}
}
