<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserChampionnatTable extends Migration {

	public function up()
	{
		Schema::create('user_championnat', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('championnat_id')->unsigned();
			$table->integer('status')->unsigned(); // status de l'invitation = 1 -> acceptée | 2 -> en attente | 3 -> refusée
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('user_championnat');
	}
}
