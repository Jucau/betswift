<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		// Schema::table('match', function(Blueprint $table) {
		// 	$table->foreign('entite_id_1')->references('id')->on('entite')
		// 				->onDelete('restrict')
		// 				->onUpdate('restrict');
		// });
		// Schema::table('match', function(Blueprint $table) {
		// 	$table->foreign('entite_id_2')->references('id')->on('entite')
		// 				->onDelete('restrict')
		// 				->onUpdate('restrict');
		// });
		Schema::table('match', function(Blueprint $table) {
			$table->foreign('journee_id')->references('id')->on('journee')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('pari', function(Blueprint $table) {
			$table->foreign('match_id')->references('id')->on('match')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('pari', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('championnat', function(Blueprint $table) {
			$table->foreign('created_by')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('journee', function(Blueprint $table) {
			$table->foreign('championnat_id')->references('id')->on('championnat')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('journee', function(Blueprint $table) {
			$table->foreign('created_by')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('score_final', function(Blueprint $table) {
			$table->foreign('match_id')->references('id')->on('match')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('user_championnat', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('user_championnat', function(Blueprint $table) {
			$table->foreign('championnat_id')->references('id')->on('championnat')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		// Schema::table('match', function(Blueprint $table) {
		// 	$table->dropForeign('match_entite_id_1_foreign');
		// });
		// Schema::table('match', function(Blueprint $table) {
		// 	$table->dropForeign('match_entite_id_2_foreign');
		// });
		Schema::table('match', function(Blueprint $table) {
			$table->dropForeign('match_journee_id_foreign');
		});
		Schema::table('pari', function(Blueprint $table) {
			$table->dropForeign('pari_match_id_foreign');
		});
		Schema::table('pari', function(Blueprint $table) {
			$table->dropForeign('pari_user_id_foreign');
		});
		Schema::table('championnat', function(Blueprint $table) {
			$table->dropForeign('championnat_created_by_foreign');
		});
		Schema::table('journee', function(Blueprint $table) {
			$table->dropForeign('journee_championnat_id_foreign');
		});
		Schema::table('journee', function(Blueprint $table) {
			$table->dropForeign('journee_created_by_foreign');
		});
		Schema::table('score_final', function(Blueprint $table) {
			$table->dropForeign('score_final_match_id_foreign');
		});
		Schema::table('user_championnat', function(Blueprint $table) {
			$table->dropForeign('user_championnat_user_id_foreign');
		});
		Schema::table('user_championnat', function(Blueprint $table) {
			$table->dropForeign('user_championnat_championnat_id_foreign');
		});
	}
}
