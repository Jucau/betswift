<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatchTable extends Migration {

	public function up()
	{
		Schema::create('match', function(Blueprint $table) {
			$table->increments('id');
			$table->string('entite_id_1');
			$table->string('entite_id_2');
			$table->integer('journee_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('match');
	}
}
