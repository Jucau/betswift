<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateScoreFinalTable extends Migration {

	public function up()
	{
		Schema::create('score_final', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('match_id')->unsigned();
			$table->timestamps();
			$table->integer('resultat');
		});
	}

	public function down()
	{
		Schema::drop('score_final');
	}
}
