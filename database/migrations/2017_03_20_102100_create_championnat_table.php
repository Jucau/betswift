<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChampionnatTable extends Migration {

	public function up()
	{
		Schema::create('championnat', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nom');
			$table->timestamps();
			$table->integer('created_by')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('championnat');
	}
}
