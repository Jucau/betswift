<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OAuthTableSeeder::class);
        $this->call(ChampionnatTableSeeder::class);
        $this->call(UserChampionnatTableSeeder::class);
        // $this->call(EntiteTableSeeder::class);
        $this->call(JourneeTableSeeder::class);
        $this->call(MatchTableSeeder::class);
        $this->call(ScoreTableSeeder::class);
        $this->call(PariTableSeeder::class);
    }
}
