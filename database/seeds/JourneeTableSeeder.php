<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class JourneeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

      public function run()
      {
          DB::table('journee')->insert([
              'id' => '1',
              'nom' => 'Première journée',
              'date_limite' => Carbon::createFromDate(2017, 3, 20, config('app.timezone')),
              'championnat_id' => '1',
              'created_by' => '1',
          ]);
          DB::table('journee')->insert([
              'id' => '2',
              'nom' => 'Journée de la mort',
              'date_limite' => Carbon::createFromDate(2017, 4, 15, config('app.timezone')),
              'championnat_id' => '1',
              'created_by' => '2',
          ]);
          DB::table('journee')->insert([
              'id' => '3',
              'nom' => 'Journée classico',
              'date_limite' => Carbon::createFromDate(2017, 4, 7, config('app.timezone')),
              'championnat_id' => '1',
              'created_by' => '1',
          ]);

      }
}
