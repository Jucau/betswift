<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserChampionnatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_championnat')->insert([
            'user_id' => '1',
            'championnat_id' => '1',
            'status' => '1',
        ]);
        DB::table('user_championnat')->insert([
            'user_id' => '2',
            'championnat_id' => '1',
            'status' => '1',
        ]);
        DB::table('user_championnat')->insert([
            'user_id' => '4',
            'championnat_id' => '1',
            'status' => '2',
        ]);
        DB::table('user_championnat')->insert([
            'user_id' => '1',
            'championnat_id' => '2',
            'status' => '2',
        ]);
        DB::table('user_championnat')->insert([
            'user_id' => '3',
            'championnat_id' => '2',
            'status' => '2',
        ]);
        DB::table('user_championnat')->insert([
            'user_id' => '3',
            'championnat_id' => '1',
            'status' => '2',
        ]);
        DB::table('user_championnat')->insert([
            'user_id' => '2',
            'championnat_id' => '3',
            'status' => '2',
        ]);
        DB::table('user_championnat')->insert([
            'user_id' => '4',
            'championnat_id' => '3',
            'status' => '3',
        ]);


    }
}
