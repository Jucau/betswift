<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ChampionnatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('championnat')->insert([
            'id' => '1',
            'nom' => 'Mon championnat',
            'created_by' => '1',
        ]);
        DB::table('championnat')->insert([
            'id' => '2',
            'nom' => 'La ligue du siècle',
            'created_by' => '2',
        ]);
        DB::table('championnat')->insert([
            'id' => '3',
            'nom' => 'Combats de tortue',
            'created_by' => '1',
        ]);


    }
}
