<?php

use Illuminate\Database\Seeder;

class MatchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('match')->insert([
          'id' => '1',
          'entite_id_1' => 'Lyon',
          'entite_id_2' => 'PSG',
          'journee_id' => '1',
      ]);
      DB::table('match')->insert([
          'id' => '2',
          'entite_id_1' => 'Lille',
          'entite_id_2' => 'Monaco',
          'journee_id' => '1',
      ]);
      DB::table('match')->insert([
          'id' => '3',
          'entite_id_1' => 'Marseille',
          'entite_id_2' => 'Nantes',
          'journee_id' => '1',
      ]);
      DB::table('match')->insert([
          'id' => '4',
          'entite_id_1' => 'Real Madrid',
          'entite_id_2' => 'Man City',
          'journee_id' => '1',
      ]);
      DB::table('match')->insert([
          'id' => '5',
          'entite_id_1' => 'Man Utd',
          'entite_id_2' => 'Bayern',
          'journee_id' => '1',
      ]);
      DB::table('match')->insert([
          'id' => '6',
          'entite_id_1' => 'Dortmund',
          'entite_id_2' => 'Bordeaux',
          'journee_id' => '1',
      ]);
      DB::table('match')->insert([
          'id' => '7',
          'entite_id_1' => 'Milan',
          'entite_id_2' => 'Juve',
          'journee_id' => '1',
      ]);
      DB::table('match')->insert([
          'id' => '8',
          'entite_id_1' => 'Roma',
          'entite_id_2' => 'Seville',
          'journee_id' => '2',
      ]);
      DB::table('match')->insert([
          'id' => '9',
          'entite_id_1' => 'Atletico',
          'entite_id_2' => 'Valence',
          'journee_id' => '2',
      ]);
      DB::table('match')->insert([
          'id' => '10',
          'entite_id_1' => 'Arsenal',
          'entite_id_2' => 'Chelsea',
          'journee_id' => '2',
      ]);
      DB::table('match')->insert([
          'id' => '11',
          'entite_id_1' => 'Bordeaux',
          'entite_id_2' => 'Real Madrid',
          'journee_id' => '2',
      ]);
      DB::table('match')->insert([
          'id' => '12',
          'entite_id_1' => 'Lyon',
          'entite_id_2' => 'Marseille',
          'journee_id' => '2',
      ]);
      DB::table('match')->insert([
          'id' => '13',
          'entite_id_1' => 'Arles Avignon',
          'entite_id_2' => 'Lens',
          'journee_id' => '2',
      ]);
      DB::table('match')->insert([
          'id' => '14',
          'entite_id_1' => 'Real Madrid',
          'entite_id_2' => 'FC Barcelone',
          'journee_id' => '3',
      ]);
      DB::table('match')->insert([
          'id' => '15',
          'entite_id_1' => 'AS Roma',
          'entite_id_2' => 'Man Utd',
          'journee_id' => '3',
      ]);
      DB::table('match')->insert([
          'id' => '16',
          'entite_id_1' => 'Dortmund',
          'entite_id_2' => 'Leverkusen',
          'journee_id' => '3',
      ]);
      DB::table('match')->insert([
          'id' => '17',
          'entite_id_1' => 'Ajax',
          'entite_id_2' => 'Porto',
          'journee_id' => '3',
      ]);
      DB::table('match')->insert([
          'id' => '18',
          'entite_id_1' => 'Liverpool',
          'entite_id_2' => 'Everton',
          'journee_id' => '3',
      ]);
    }
}
