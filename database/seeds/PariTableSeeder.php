<?php

use Illuminate\Database\Seeder;

class PariTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('pari')->insert([
          'id' => '1',
          'match_id' => '1',
          'resultat' => '3',
          'user_id' => '1',
      ]);
      DB::table('pari')->insert([
          'id' => '2',
          'match_id' => '2',
          'resultat' => '1',
          'user_id' => '1',
      ]);
      DB::table('pari')->insert([
          'id' => '3',
          'match_id' => '3',
          'resultat' => '2',
          'user_id' => '1',
      ]);
      DB::table('pari')->insert([
          'id' => '4',
          'match_id' => '4',
          'resultat' => '3',
          'user_id' => '1',
      ]);
      DB::table('pari')->insert([
          'id' => '5',
          'match_id' => '5',
          'resultat' => '1',
          'user_id' => '1',
      ]);
      DB::table('pari')->insert([
          'id' => '6',
          'match_id' => '6',
          'resultat' => '3',
          'user_id' => '1',
      ]);
      DB::table('pari')->insert([
          'id' => '7',
          'match_id' => '7',
          'resultat' => '1',
          'user_id' => '1',
      ]);


      DB::table('pari')->insert([
          'id' => '8',
          'match_id' => '1',
          'resultat' => '1',
          'user_id' => '2',
      ]);
      DB::table('pari')->insert([
          'id' => '9',
          'match_id' => '2',
          'resultat' => '3',
          'user_id' => '2',
      ]);
      DB::table('pari')->insert([
          'id' => '10',
          'match_id' => '3',
          'resultat' => '1',
          'user_id' => '2',
      ]);
      DB::table('pari')->insert([
          'id' => '11',
          'match_id' => '4',
          'resultat' => '2',
          'user_id' => '2',
      ]);
      DB::table('pari')->insert([
          'id' => '12',
          'match_id' => '5',
          'resultat' => '1',
          'user_id' => '2',
      ]);
      DB::table('pari')->insert([
          'id' => '13',
          'match_id' => '6',
          'resultat' => '1',
          'user_id' => '2',
      ]);
      DB::table('pari')->insert([
          'id' => '14',
          'match_id' => '7',
          'resultat' => '2',
          'user_id' => '2',
      ]);
    }
}
