<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EntiteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('entite')->insert([
            'id' => '1',
            'nom' => 'Lyon'
        ]);
        DB::table('entite')->insert([
            'id' => '2',
            'nom' => 'PSG',
        ]);
        DB::table('entite')->insert([
            'id' => '3',
            'nom' => 'Marseille',
        ]);
        DB::table('entite')->insert([
            'id' => '4',
            'nom' => 'Nantes',
        ]);
        DB::table('entite')->insert([
            'id' => '5',
            'nom' => 'Lille',
        ]);
        DB::table('entite')->insert([
            'id' => '6',
            'nom' => 'Monaco',
        ]);
        DB::table('entite')->insert([
            'id' => '7',
            'nom' => 'Real Madrid',
        ]);
        DB::table('entite')->insert([
            'id' => '8',
            'nom' => 'Man City',
        ]);
        DB::table('entite')->insert([
            'id' => '9',
            'nom' => 'Man Utd',
        ]);
        DB::table('entite')->insert([
            'id' => '10',
            'nom' => 'Bayern',
        ]);
        DB::table('entite')->insert([
            'id' => '11',
            'nom' => 'Dortmund',
        ]);
        DB::table('entite')->insert([
            'id' => '12',
            'nom' => 'Bordeaux',
        ]);
        DB::table('entite')->insert([
            'id' => '13',
            'nom' => 'Milan',
        ]);
        DB::table('entite')->insert([
            'id' => '14',
            'nom' => 'Juve',
        ]);
        DB::table('entite')->insert([
            'id' => '15',
            'nom' => 'Roma',
        ]);
        DB::table('entite')->insert([
            'id' => '16',
            'nom' => 'FC Seville',
        ]);
        DB::table('entite')->insert([
            'id' => '17',
            'nom' => 'Atletico',
        ]);
        DB::table('entite')->insert([
            'id' => '18',
            'nom' => 'Valence',
        ]);
        DB::table('entite')->insert([
            'id' => '19',
            'nom' => 'Arsenal',
        ]);
        DB::table('entite')->insert([
            'id' => '20',
            'nom' => 'Chelsea',
        ]);


    }
}
