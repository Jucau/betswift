<?php


use Illuminate\Hashing\BcryptHasher;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class OAuthTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
$hasher = new BcryptHasher();

        DB::table('users')->insert([
            'id' => '1',
            'name' => 'cauvin',
            'email' => 'test@test.com',
            'password' => $hasher->make('0000'),
        ]);
        DB::table('users')->insert([
            'id' => '2',
            'name' => 'ha',
            'email' => 'test2@test.com',
            'password' => $hasher->make('0000'),
        ]);
        DB::table('users')->insert([
            'id' => '3',
            'name' => 'haguet',
            'email' => 'test3@test.com',
            'password' => $hasher->make('0000'),
        ]);
        DB::table('users')->insert([
            'id' => '4',
            'name' => 'beaumont',
            'email' => 'test4@test.com',
            'password' => $hasher->make('0000'),
        ]);
        DB::table('oauth_clients')->insert([
            'id' => '1',
            'name' => ' Password Grant Client',
            'secret' => 'ZXdMNAizgOVs27EOqTil3lJGG9z4TIwDy2GdfGxR',
            'redirect' => 'http://localhost',
            'personal_access_client' => '0',
            'password_client' => '1',
            'revoked' => '0',
        ]);


    }
}
