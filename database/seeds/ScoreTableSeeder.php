<?php

use Illuminate\Database\Seeder;

class ScoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('score_final')->insert([
          'id' => '1',
          'match_id' => '1',
          'resultat' => '3',
      ]);
      DB::table('score_final')->insert([
          'id' => '2',
          'match_id' => '2',
          'resultat' => '1',
      ]);
      DB::table('score_final')->insert([
          'id' => '3',
          'match_id' => '3',
          'resultat' => '2',
      ]);
      DB::table('score_final')->insert([
          'id' => '4',
          'match_id' => '4',
          'resultat' => '3',
      ]);
      DB::table('score_final')->insert([
          'id' => '5',
          'match_id' => '5',
          'resultat' => '1',
      ]);
      DB::table('score_final')->insert([
          'id' => '6',
          'match_id' => '6',
          'resultat' => '1',
      ]);
      DB::table('score_final')->insert([
          'id' => '7',
          'match_id' => '7',
          'resultat' => '2',
      ]);
    }
}
