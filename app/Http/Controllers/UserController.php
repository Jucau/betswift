<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\UserRepository;
use App\Http\Requests\UserRequest;

class UserController extends MainController {

  protected $userRepository;

  public function __construct(UserRepository $userRepository)
  {
		$this->userRepository = $userRepository;
	}

  public function showAll()
  {
    $users = $this->userRepository->findAll();
    return $users;
  }

  public function showOne($id)
  {
    $user = $this->userRepository->getById($id);
    if($user) {
      $this->success['data'] = $user;
      return $this->success;
    }
    $this->error['data'] = "Aucun utilisateur correspondant à cet identifiant.";
    return $this->error;
  }

  public function getChampionnats(Request $request) {
    $mine = $this->userRepository->getChampionnats($request->user()->id);
    if($mine) {
      $this->success['data'] = $mine;
      return $this->success;
    }
    $this->error['data'] = "Une erreur est survenue.";
    return $this->error;
  }

  public function create(UserRequest $request) {
    if($this->userRepository->create($request->all())) {
      $this->success['data'] = "Utilisateur créé";
      return $this->success;
    }
    $this->error['data'] = "Une erreur est survenue.";
    return $this->error;
  }



}
