<?php

namespace App\Http\Controllers;

class MainController extends Controller {

  public $success;
  public $error;

  public function __construct() {
    $this->success = ['error' => false];
    $this->error = ['error' => true];
  }

}
