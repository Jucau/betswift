<?php

namespace App\Http\Controllers;


use App\Http\Requests\ChampionnatRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Repository\ChampionnatRepository;

class ChampionnatController extends MainController {

  public $championnatRepository;

  public function __construct(ChampionnatRepository $championnatRepository){
    parent::__construct();
    $this->championnatRepository = $championnatRepository;
  }

  public function getMine(Request $request) {
    $mine = $this->championnatRepository->getMine($request->user()->id);
    if($mine) {
      $this->success['data'] = $mine;
      return $this->success;
    }
    $this->error['data'] = "Une erreur est survenue.";
    return $this->error;
  }

  public function getInvitations(Request $request) {
    $invitations = $this->championnatRepository->getInvitations($request->user()->id);
    if($invitations) {
      $this->success['data'] = $invitations;
      return $this->success;
    }
    $this->error['data'] = "Une erreur est survenue.";
    return $this->error;
  }

  public function getOne($id) {
    $this->success['data'] = $this->championnatRepository->getOne($id);
    return $this->success;
  }

  public function create(ChampionnatRequest $request) {
    $champ = $this->championnatRepository->create($request->all(), $request->user()->id);
    if ($champ) {
      $this->success['data'] = $champ;
      return $this->success;
    }
    $this->error['data'] = "Problème à l\'enregistrement";
    return $this->error;
  }

  public function getJournees($id) {
    $journees = $this->championnatRepository->getJournees($id);
    if($journees) {
      $this->success['data'] = $journees;
      return $this->success;
    }
    $this->error['data'] = "Une erreur est survenue.";
    return $this->error;
  }

  public function getUsers($id) {
      $users = $this->championnatRepository->getUsers($id);
      if($users) {
        $this->success['data'] = $users;
        return $this->success;
      }
      $this->error['data'] = "Une erreur est survenue.";
      return $this->error;
  }

  public function createJournee($id, ChampionnatRequest $request) {
    $journee = $this->championnatRepository->createJournee($request, $request->user()->id, $id);
    if($journee) {
      $this->success['data'] = $journee;
      return $this->success;
    }
    $this->error['data'] = "Une erreur est survenue.";
    return $this->error;
  }

  public function getScores($id) {
      $score = $this->championnatRepository->getScores($id);
      if($score) {
        $this->success['data'] = $score;
        return $this->success;
      }
      $this->error['data'] = "Une erreur est survenue.";
      return $this->error;
  }

  public function invitePlayer(Request $request, $id) {
    $invit = $this->championnatRepository->invitePlayer($request->all(), $id);
    if($invit) {
      $this->success['data'] = "Utilisateur bien invité";
      return $this->success;
    }
    $this->error['data'] = "Une erreur est survenue.";
    return $this->error;
  }

  public function acceptInvit(Request $request) {
    $accept = $this->championnatRepository->manageInvit($request->user()->id, $request->all(), 1);
    if($accept) {
      $this->success['data'] = "Invitation acceptée";
      return $this->success;
    }
    $this->error['data'] = "Une erreur est survenue.";
    return $this->error;
  }

  public function refuseInvit(Request $request) {
    $accept = $this->championnatRepository->manageInvit($request->user()->id, $request->all(), 3);
    if($accept) {
      $this->success['data'] = "Invitation refusée";
      return $this->success;
    }
    $this->error['data'] = "Une erreur est survenue.";
    return $this->error;
  }

}
