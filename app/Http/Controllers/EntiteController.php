<?php

namespace App\Http\Controllers;

use App\Repository\EntiteRepository;
use App\Http\Requests\EntiteRequest;

class EntiteController extends MainController {

  public $entiteRepository;

  public function __construct(EntiteRepository $entiteRepository) {
    $this->entiteRepository = $entiteRepository;
  }

  public function create(EntiteRequest $request) {
    if($this->entiteRepository->create($request->all())) {
      $this->success['data'] = "Entité créée";
      return $this->success;
    }
    $this->error['data'] = "Une erreur est survenue.";
    return $this->error;
  }

}
