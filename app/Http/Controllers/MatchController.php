<?php

namespace App\Http\Controllers;

use App\Repositories\MatchRepository;
use App\Http\Requests\PariRequest;
use App\Http\Requests\ScoreRequest;

class MatchController extends MainController {

  public function __construct(MatchRepository $matchRepository)
	{
      $this->matchRepository = $matchRepository;
	}

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   * match/{id}/pari
   */
   public function storePari(PariRequest $request, $id)
   {
     if ($this->matchRepository->storePari($request->all(), $id)) {
       $this->success['data'] = "Votre pari a bien été ajouté !";
       return $this->success;
     }
     $this->error['data'] = "Problème à l\'enregistrement";
     return $this->error;
   }

   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    * match/{id}/score
    */
    public function storeScore(ScoreRequest $request, $id)
    {
      if ($this->matchRepository->storeScore($request->all(), $id)) {
        $this->success['data'] = "vous venez d'ajouté un score !";
        return $this->success;
      }
      $this->error['data'] = "Problème à l\'enregistrement";
      return $this->error;
    }

}

?>
