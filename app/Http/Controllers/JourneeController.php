<?php

namespace App\Http\Controllers;

use App\Repositories\JourneeRepository;
use App\Http\Requests\MatchRequest;

class JourneeController extends MainController {

  public function __construct(JourneeRepository $journeeRepository)
	{
      parent::__construct();
		  $this->journeeRepository = $journeeRepository;
	}

  /**
   * Display a listing of the resource.
   *
   * @return Response
   * journee
   */
  public function showAll()
  {
    return $this->journeeRepository->findAll();
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   * journee/{id}/match
   */
   public function storeMatch(MatchRequest $request, $id)
   {
     if ($this->journeeRepository->storeMatch($request->all(), $id)) {
       $this->success['data'] = "Journée ajoutée !";
       return $this->success;
     }
     $this->error['data'] = "Problème à l\'enregistrement";
     return $this->error;
   }


  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   * journee/{id}
   */
  public function showOne($id)
  {
    return $this->journeeRepository->getById($id);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   * journee/{id}/match
   */
  public function showMatchs($id)
  {
    return $this->journeeRepository->findMatchs($id);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    return $this->journeeRepository->delete($id);
  }

  public function getScores($id) {
    $scores = $this->journeeRepository->getScores($id);
    if($scores) {
      $this->success['data'] = $scores;
      return $this->success;
    }
    $this->error['data'] = "Une erreur est survenue.";
    return $this->error;
  }

}
