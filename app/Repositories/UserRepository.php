<?php

namespace App\Repositories;

use App\User;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Requests\Request;
use DB;
use Hash;

class UserRepository
{


	public function __construct(User $user)
	{
		  $this->user = $user;
	}

	public function findAll()
	{
      return $this->user->all();
	}

	public function getById($id)
	{
		$user = $this->user->find($id);
		if($user) {
			return $this->user->find($id)->with('championnat')->first();
		}
	  return false;
	}

	public function destroy($id)
	{
	    return $this->getById($id)->delete();
	}

	public function getChampionnats($id) {
		$user = $this->user->find($id);
		if($user) {
    	return $user->championnat()->get();
		}
		return false;
	}

	public function create($inputs) {
		$user = new User();
		$user->name = $inputs['name'];
		$user->password = Hash::make($inputs['password']);
		$user->email = $inputs['email'];
		return $user->save();
	}
}
