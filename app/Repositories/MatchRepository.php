<?php

namespace App\Repositories;

use App\Model\Match;
use App\Model\Pari;
use App\Model\ScoreFinal;
use Illuminate\Database\Eloquent\Collection;
use DB;
use Auth;

class MatchRepository
{

	public function __construct(Match $match, Pari $pari, ScoreFinal $scoreFinal)
	{
		$this->match = $match;
		$this->pari = $pari;
		$this->scoreFinal = $scoreFinal;
	}

	public function storePari($inputs, $id)
	{
		$user = Auth::user();
		$match = Match::find($id);
		if(!empty($match)){
			$this->pari->resultat = $inputs['resultat'];
			$this->pari->user()->associate($user);
			$this->pari->match_id = $id;
		}
		return $this->pari->save();
	}

	public function storeScore($inputs, $id)
	{
		$match = Match::find($id);
		$this->scoreFinal->resultat = $inputs['resultat'];
		$this->scoreFinal->match()->associate($match);
		return $this->scoreFinal->save();
	}

	public function delete($id)
	{
    return $this->getById($id)->delete();
	}
}
