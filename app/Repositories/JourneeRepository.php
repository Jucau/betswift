<?php

namespace App\Repositories;

use App\Model\Journee;
use App\Model\Match;
use App\Model\ScoreFinal;
// use App\Model\Entite;
use App\Model\Pari;
use App\Model\Championnat;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use DB;
use Auth;

class JourneeRepository
{


	public function __construct(Journee $journee, Match $match, User $user)
	{
	  $this->journee = $journee;
		$this->match = $match;
		$this->user = $user;
	}

	public function findAll()
	{
    return $this->journee->with('match')->get();
	}

	public function storeMatch($inputs, $id)
	{
		// $entite1 = Entite::find($inputs['entite_id_1']);
		// $entite2 = Entite::find($inputs['entite_id_2']);
		$journee = Journee::find($id);
		//
		// $this->match->entite1()->associate($entite1);
		// $this->match->entite2()->associate($entite2);
		$this->match->journee()->associate($journee);
		$this->match->entite_id_1 = $inputs['entite_id_1'];
		$this->match->entite_id_2 = $inputs['entite_id_2'];
		return $this->match->save();
	}

	public function getById($id)
	{
    return $this->journee->with('match')->find($id);
	}

	public function findMatchs($id)
	{
		return $this->journee->find($id)
				->with(['match' => function ($query) {
				    $query->with(['score', 'pari' => function($q) {
							$q->where('user_id', '=', Auth::user()->id);
						}]);
				}])->get();
	}


	public function delete($id)
	{
    return $this->getById($id)->delete();
	}

	public function getScores($id) {
		$journee = $this->journee->find($id);
		if($journee) {
	    $userResults = array();
	    $users = Championnat::find($journee->championnat_id)->user()->get();
			if(new Carbon($journee->date_limite) < Carbon::now()) {
				$matches = Match::where('journee_id', '=', $journee->id)->get();
				$validScores = array();
				foreach($matches as $match) { // foreach chaque match
					$score = ScoreFinal::where('match_id', '=', $match->id)->first();
					if($score !== null) {
						$validScores[$score->match_id] = $score->resultat;
						foreach($users as $user) { // foreach chaque user
							$pariUser = Pari::where([['user_id', '=', $user->id],['match_id', '=', $match->id]])->first();
							if($pariUser) {
								if($score->resultat == $pariUser->resultat) {
									if(!array_key_exists($user->name, $userResults)) {
										$userResults[$user->name] = array();
									}
									if(!array_key_exists($journee->id, $userResults[$user->name])) {
										$userResults[$user->name][$journee->id] = '0';
									}
									$userResults[$user->name][$journee->id] = $userResults[$user->name][$journee->id] + 1;
								}
							}
						}
					}
				}
			}
      foreach($userResults as $key => $userResult) {
        $userScore = 0;
        foreach($userResult as $res) {
          $userScore = $userScore + $res;
        }
        $userResults[$key] = $userScore;
      }
      arsort($userResults);
      return $userResults;
		}
		return false;
	}

}
