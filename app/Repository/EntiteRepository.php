<?php

namespace App\Repository;

use App\Model\Entite;

class EntiteRepository
{

  public function nameExists($name) {
    if(Entite::where('nom', '=', $name)->first()) {
      return true;
    }
    return false;
  }

  public function create($inputs) {
    if(!$this->nameExists($inputs['nom'])) {
      $entite = new Entite();
      $entite->nom = $inputs['nom'];
      return $entite->save();
    }
    return false;
  }

}
