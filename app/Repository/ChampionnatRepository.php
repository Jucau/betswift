<?php

namespace App\Repository;

use App\Model\Championnat;
use App\Model\Journee;
use App\Model\ScoreFinal;
use App\Model\Match;
use App\Model\Pari;
use App\User;
use Carbon\Carbon;
use DB;

class ChampionnatRepository {

  public $championnat;

  public function __construct(Championnat $championnat) {
    $this->championnat = $championnat;
  }

  public function getMine($id) {
    return User::find($id)->championnat()->where('status', '=', 1)->get();
  }

  public function getInvitations($id) {
    return User::find($id)->championnat()->where('status', '=', 2)->get();
  }

  public function getOne($id){
    return $this->championnat->findOrFail($id);
  }

  public function create($input, $id) {
    $championnat = new Championnat();
    $championnat->nom = $input['nom'];
    $championnat->created_at = Carbon::now();
    $championnat->created_by = $id;
    $championnat->save();
    // dd($championnat->id);
    User::find($id)->championnat()->attach($championnat->id, ['status' => '1']);
    return $championnat;
  }

  public function getJournees($id) {
    if($this->championnat->find($id)) {
      return $this->championnat->find($id)->journee()->get();
    }
    return false;
  }

  public function getUsers($id) {
    if($this->championnat->find($id)) {
      return $this->championnat->find($id)->user()->get();
    }
    return false;
  }

  public function createJournee($inputs, $idUser, $idChampionnat) {
    $journee = new Journee();
    $journee->nom = $inputs['nom'];
    $journee->date_limite = $inputs['date_limite'];
    $journee->created_by = $idUser;
    $journee->championnat_id = $idChampionnat;
    $journee->save();
    return $journee;
  }

  public function searchUserByName($name) {
    $user = User::where('name', '=', $name)->get();
    if(!$user->isEmpty()) {
      return $user[0];
    }
    return false;
  }

  public function invitePlayer($inputs, $id){
    $user = $this->searchUserByName($inputs['name']);
    if($user) {
      User::find($user->id)->championnat()->detach($id);
      User::find($user->id)->championnat()->attach($id, ['status' => '2']);
      return true;
    }
    return false;
  }

  public function manageInvit($userId, $inputs, $status){
    $user = User::find($userId);
    if($user) {
      User::find($userId)->championnat()->detach($inputs['championnat_id']);
      User::find($userId)->championnat()->attach($inputs['championnat_id'], ['status' => $status]);
      return true;
    }
    return false;
  }

  public function getScores($id) {
    $championnat = $this->championnat->find($id);
    if ($championnat) {
      $userResults = array();
      $journees = $championnat->journee()->get();
      $users = $championnat->user()->get();
      foreach($journees as $journee) { // foreach chaque journee
        if(new Carbon($journee->date_limite) < Carbon::now()) {
          $matches = Match::where('journee_id', '=', $journee->id)->get();
          $validScores = array();
          foreach($matches as $match) { // foreach chaque match
            $score = ScoreFinal::where('match_id', '=', $match->id)->first();
            if($score !== null) {
              $validScores[$score->match_id] = $score->resultat;
              foreach($users as $user) { // foreach chaque user
                $pariUser = Pari::where([['user_id', '=', $user->id],['match_id', '=', $match->id]])->first();
                if($pariUser) {
                  if($score->resultat == $pariUser->resultat) {
                    if(!array_key_exists($user->name, $userResults)) {
                      $userResults[$user->name] = array();
                    }
                    if(!array_key_exists($journee->id, $userResults[$user->name])) {
                      $userResults[$user->name][$journee->id] = '0';
                    }
                    $userResults[$user->name][$journee->id] = $userResults[$user->name][$journee->id] + 1;
                  }
                }
              }
            }
          }
        }
      }
      foreach($userResults as $key => $userResult) {
        $userScore = 0;
        foreach($userResult as $res) {
          $userScore = $userScore + $res;
        }
        $userResults[$key] = $userScore;
      }
      return $userResults;
    }
    arsort($userResults);
    return false;
  }



}
