<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Entite extends Model {

	protected $table = 'entite';
	public $timestamps = true;

	public function match()
	{
		return $this->hasMany('App\Model\Match');
	}

}