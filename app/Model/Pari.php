<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pari extends Model {

	protected $table = 'pari';
	public $timestamps = true;

	public function match()
	{
		return $this->hasOne('App\Model\Match');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

}
