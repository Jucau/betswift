<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Journee extends Model {

	protected $table = 'journee';
	public $timestamps = true;
	protected $fillable = ['nom', 'date_limite'];

	public function championnat()
	{
		return $this->belongsTo('App\Model\Championnat');
	}

	public function match()
	{
		return $this->hasMany('App\Model\Match');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

}
