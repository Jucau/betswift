<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ScoreFinal extends Model {

	protected $table = 'score_final';
	public $timestamps = true;

	public function match()
	{
		return $this->belongsTo('App\Model\Match');
	}

}