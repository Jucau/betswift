<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Championnat extends Model {

	protected $table = 'championnat';
	public $timestamps = true;

	public function journee()
	{
		return $this->hasMany('App\Model\Journee');
	}

	public function user()
	{
		return $this->belongsToMany('App\User', 'user_championnat', 'championnat_id', 'user_id');
	}

}
