<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Match extends Model {

	protected $table = 'match';
	public $timestamps = true;

	public function journee()
	{
		return $this->belongsTo('App\Model\Journee');
	}

	public function pari()
	{
		return $this->hasMany('App\Model\Pari');
	}

	public function user()
	{
		return $this->hasOne('App\User');
	}

	public function score()
	{
		return $this->hasOne('App\Model\ScoreFinal');
	}

	public function entite1()
	{
		return $this->belongsTo('App\Model\Entite', 'entite_id_1');
	}

	public function entite2()
	{
		return $this->belongsTo('App\Model\Entite', 'entite_id_2');
	}

}
