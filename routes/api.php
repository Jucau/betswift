<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('user', 'UserController@create');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/', function ()    {
        // Uses Auth Middleware
        return 'yeah';
    });

    Route::get('user/profile', function () {
        // Uses Auth Middleware
    });


    Route::get('championnat', 'ChampionnatController@getMine');
    Route::get('championnat/invit', 'ChampionnatController@getInvitations');
    Route::get('championnat/{id}', 'ChampionnatController@getOne');
    Route::post('championnat', 'ChampionnatController@create');
    Route::get('championnat/{id}/journee', 'ChampionnatController@getJournees');
    Route::get('championnat/{id}/user', 'ChampionnatController@getUsers');
    Route::get('championnat/{id}/score', 'ChampionnatController@getScores');
    Route::post('championnat/{id}/journee', 'ChampionnatController@createJournee');
    Route::post('championnat/{id}/invit', 'ChampionnatController@invitePlayer');

    Route::post('match/{id}/pari', 'MatchController@storePari');
    Route::post('match/{id}/score', 'MatchController@storeScore');

    Route::get('journee', 'JourneeController@showAll');
    Route::post('journee/{id}/match', 'JourneeController@storeMatch');
    Route::get('journee/{id}', 'JourneeController@showOne');
    Route::get('journee/{id}/match', 'JourneeController@showMatchs');
    Route::delete('journee/{id}/match', 'JourneeController@storeMatch');
    Route::get('journee/{id}/score', 'JourneeController@getScores');

    Route::get('user', 'UserController@showAll');
    Route::get('user/{id}', 'UserController@showOne');
    Route::get('user/{id}/championnat', 'UserController@getChampionnats');
    Route::post('user', 'UserController@store');
    Route::post('user/accept-invit', 'ChampionnatController@acceptInvit');
    Route::post('user/refuse-invit', 'ChampionnatController@refuseInvit');

    // Route::post('entite', 'EntiteController@create');
});
